#!/bin/bash

set -eo pipefail
if [ -n "$DEBUG" ]; then
    set -x
fi

function cleanup {
    git checkout "$SCRIPT_PATH"/secrets 2>&1 >/dev/null
}
trap cleanup EXIT

SCRIPT_PATH=$(dirname "$0")
COMMAND="$1"
shift

function sops {
    find "$SCRIPT_PATH"/secrets -type f -exec sops -d -i {} \;
}

function help {
    echo "ansible | ansible-playbook"
    echo "ansible-vault"
}

function ansible_vault {
    ARGUMENT=$1
    shift
    $COMMAND "$ARGUMENT" \
        --vault-password-file "$SCRIPT_PATH"/secrets/vault_password_file "$*"
}

function run {
    $COMMAND \
        --vault-password-file "$SCRIPT_PATH"/secrets/vault_password_file \
        "$@"
}

case $COMMAND in
"ansible-vault")
    sops
    ansible_vault "$@"
    ;;
"ansible" | "ansible-playbook")
    sops
    run "$@"
    ;;
"help" | "--help")
    help
    ;;
*)
    help
    ;;
esac
